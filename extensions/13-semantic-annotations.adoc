[[im_semantic_annotations_extension]]
=== GeoPackage Semantic Annotations Extension

[WARNING]
====
This subsection is under discussion and may change radically.
====

[float]
==== Extension Title
Semantic Annotations Extension

[float]
==== Introduction
A semantic annotation is a semantically grounded term that can be applied to another concept.
Use of this extension enables semantic annotations to be applied to any business object in the current GeoPackage.

[float]
==== Extension Author
Image Matters LLC, in collaboration with the participants of OGC Testbed-15 and Testbed-16.

[float]
==== Extension Name or Template
`im_semantic_annotations` (will become `gpkg_semantic_annotations` if adopted by the OGC)

[float]
==== Extension Type
New requirement optionally dependent on the link:http://www.geopackage.org/spec121/#extension_schema[GeoPackage Schema Extension].

[float]
==== Applicability
This extension can be applied to any GeoPackage business object (layers, features, tiles, styles, etc.).

[float]
==== Scope
read-write

[float]
==== Specification
[float]
===== `gpkg_extensions`
To use this extension, add the following rows to this table in addition to the rows required for the Schema Extension (if used).

[[im_semantic_annotations_ger_table]]
.gpkg_extensions table row
[cols=",,,,",options="header",]
|====
| table_name | column_name | extension_name | definition | scope
|`gpkgext_semantic_annotations` |null |`im_semantic_annotations` |_a reference to this file_ |`read-write`
|`gpkgext_sa_reference` |null |`im_semantic_annotations` |_a reference to this file_ |`read-write`
|====

[NOTE]
==========
The values in the `definition` column SHOULD refer in some human-readable way to this extension specification. If the extension is adopted by OGC, it will gain the "gpkg_" prefix and get a different definition permalink.
==========

[float]
===== New Table Definitions
Following are definitions of the tables for this extension.
As with other GeoPackage tables, this extension takes no position on how either of these tables are to be used by a client.

[[gpkgext_semantic_annotations]]
[float]
====== `gpkgext_semantic_annotations`
When this extension is in use, add a table with this name and the following columns:

* `id` is a primary key
* `type` is a semantically grounded type (category) for the annotationfootnote:[If the semantic annotation represents a metadata profile, then the extension name for the metadata profile should be used as the type.]
* `title` is a human-readable title for the annotation
* `description` is an optional human-readable text description for the annotation
* `uri` is the resolvable URI for the semantic concept

[[gpkgext_sa_reference]]
[float]
====== `gpkgext_sa_reference`
When this extension is in use, add a table with this name and the following columns:

* `table_name` is the name of the table containing the business object
* `key_column_name` is the name of the integer column in the specified table that acts as a key
  ** _null_ indicates that the semantic annotation refers to the entire table
  ** if no such column exists, `rowid` can be used
* `key_value` is the value of the key column that uniquely identifies the row
  ** _null_ indicates that the semantic annotation refers to the entire table
* `sa_id` is a foreign key to `gpkgext_semantic_annotations`

[WARNING]
====
Use of `rowid` should be avoided in references because rowids that are not backed by an integer primary key are not guaranteed to be maintained by SQLite after vacuum operations.
====

[float]
==== Using Semantic Annotations
To use semantic annotations, do the following:

. Add rows to `gpkgext_semantic_annotations` for every annotation you want to use.
.. Optionally, use the Schema Extension to establish an enumeration for the types and further describe those types. See http://www.geopackage.org/guidance/extensions/schema.html for more details.
. Add a row to `gpkgext_sa_reference` for every row of every table requiring the annotation. There can be a many-to-many mapping between business object rows and semantic annotations.
